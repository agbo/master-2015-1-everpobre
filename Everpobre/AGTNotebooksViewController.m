//
//  AGTNotebooksViewController.m
//  Everpobre
//
//  Created by Fernando Rodríguez Romero on 08/04/15.
//  Copyright (c) 2015 Agbo. All rights reserved.
//

#import "AGTNotebooksViewController.h"
#import "AGTNotebook.h"
#import "AGTNotesViewController.h"
#import "AGTNote.h"

@interface AGTNotebooksViewController ()

@end

@implementation AGTNotebooksViewController

-(void) viewDidLoad{
    [super viewDidLoad];
    self.title = @"EverPobre";
    
    [self addNewNotebookButton];
    
    // Edit button
    self.navigationItem.leftBarButtonItem = self.editButtonItem;
    
}

-(UITableViewCell *)tableView:(UITableView *)tableView
        cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    // Averiguar cual es la libreta
    AGTNotebook *nb = [self.fetchedResultsController
                       objectAtIndexPath:indexPath];
    
    // Crear una celda
    static NSString *cellID = @"notebookCell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellID];
    if (cell == nil) {
        cell = [[UITableViewCell alloc]
                initWithStyle:UITableViewCellStyleValue1
                reuseIdentifier:cellID];
    }
    
    // Configurarla (sincronizar libreta -> celda)
    cell.textLabel.text = nb.name;
    cell.detailTextLabel.text = [NSString stringWithFormat:@"%d", [nb.notes count]];
    
    // Devolverla
    return cell;
    
    
}

-(void) tableView:(UITableView *)tableView
commitEditingStyle:(UITableViewCellEditingStyle)editingStyle
forRowAtIndexPath:(NSIndexPath *)indexPath{
    
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        
        
        // Inmediatamente lo elimino del modelo
        
        // Averiguar la libreta
        AGTNotebook *nb = [self.fetchedResultsController objectAtIndexPath:indexPath];
        
        // Eliminarla
        [self.fetchedResultsController.managedObjectContext deleteObject:nb];
        
    }
    
}

#pragma mark - Table Delegate
-(void) tableView:(UITableView *)tableView
didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    // Averiguar la libreta
    AGTNotebook *n = [self.fetchedResultsController objectAtIndexPath:indexPath];
    
    // Crear un contorlador de notas
    NSFetchRequest *req = [NSFetchRequest fetchRequestWithEntityName:[AGTNote entityName]];
    req.sortDescriptors = @[[NSSortDescriptor
                             sortDescriptorWithKey:AGTNoteAttributes.name
                             ascending:YES
                             selector:@selector(caseInsensitiveCompare:)],
                            [NSSortDescriptor
                             sortDescriptorWithKey:AGTNoteAttributes.modificationDate
                             ascending:NO]];
    req.fetchBatchSize = 20;

    req.predicate = [NSPredicate predicateWithFormat:@"notebook = %@", n];
    
    NSFetchedResultsController *fc = [[NSFetchedResultsController alloc]
                                      initWithFetchRequest:req
                                      managedObjectContext:n.managedObjectContext
                                      sectionNameKeyPath:nil cacheName:nil];
    
    
    AGTNotesViewController *nVC = [[AGTNotesViewController alloc]
                                   initWithFetchedResultsController:fc
                                   style:UITableViewStylePlain
                                   notebook:n];
    
    // Hacer un push
    [self.navigationController pushViewController:nVC
                                         animated:YES];
    
}


#pragma mark - Utils
-(void) addNewNotebookButton{
    
    UIBarButtonItem *add = [[UIBarButtonItem alloc]
                            initWithBarButtonSystemItem:UIBarButtonSystemItemAdd
                            target:self action:@selector(addNewNotebook:)];
    
    self.navigationItem.rightBarButtonItem = add;
    
}

#pragma mark -  Actions
-(void) addNewNotebook:(id) sender{
   
    [AGTNotebook notebookWithName:@"Nueva Libreta"
                          context:self.fetchedResultsController.managedObjectContext];
    
}


















@end
