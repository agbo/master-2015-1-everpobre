//
//  AppDelegate.m
//  Everpobre
//
//  Created by Fernando Rodríguez Romero on 07/04/15.
//  Copyright (c) 2015 Agbo. All rights reserved.
//

#import "AGTCoreDataStack.h"
#import "AppDelegate.h"
#import "AGTNotebook.h"
#import "AGTNote.h"
#import "AGTNotebooksViewController.h"
#import "UIViewController+Navigation.h"
#import "Settings.h"

@interface AppDelegate ()
@property (nonatomic, strong) AGTCoreDataStack *stack;
@end

@implementation AppDelegate


- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    // Override point for customization after application launch.
    
    // Creamos una instancia del stack
    self.stack = [AGTCoreDataStack coreDataStackWithModelName:@"Model"];
    
    
    // Creamos datos chorras
    //[self createDummyData];
    
    // Un fetchRequest
    NSFetchRequest *req = [NSFetchRequest fetchRequestWithEntityName:[AGTNotebook entityName]];
    req.sortDescriptors = @[[NSSortDescriptor
                            sortDescriptorWithKey:AGTNotebookAttributes.name
                            ascending:YES
                            selector:@selector(caseInsensitiveCompare:)],
                           [NSSortDescriptor
                            sortDescriptorWithKey:AGTNotebookAttributes.modificationDate
                            ascending:NO]];
    req.fetchBatchSize = 20;
    
    // FetchedResultsController
    NSFetchedResultsController *fc = [[NSFetchedResultsController alloc]
                                      initWithFetchRequest:req
                                      managedObjectContext:self.stack.context
                                      sectionNameKeyPath:nil
                                      cacheName:nil];
    
    // Creamos el controlador
    AGTNotebooksViewController *nVC = [[AGTNotebooksViewController alloc] initWithFetchedResultsController:fc style:UITableViewStylePlain];
    
    
    
    self.window = [[UIWindow alloc] initWithFrame:
                   [[UIScreen mainScreen] bounds]];
    self.window.backgroundColor = [UIColor whiteColor];
    
    self.window.rootViewController = [nVC wrappedInNavigation];
    
    [self.window makeKeyAndVisible];
    
    
    // Arranco el autosave
    [self autoSave];
    
    return YES;
}

- (void)applicationWillResignActive:(UIApplication *)application {
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
    
    [self.stack saveWithErrorBlock:^(NSError *error) {
        NSLog(@"Error al guardar: %@", error);
    }];
}

- (void)applicationDidEnterBackground:(UIApplication *)application {
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    [self.stack saveWithErrorBlock:^(NSError *error) {
        NSLog(@"Error al guardar: %@", error);
    }];
}

- (void)applicationWillEnterForeground:(UIApplication *)application {
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application {
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}

- (void)applicationWillTerminate:(UIApplication *)application {
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    NSLog(@"Adiós mundo cruel");
}


-(void) createDummyData{
    
    // Elimino datos anteriores
    [self.stack zapAllData];
    
    
    // Creamos nuevos objetos
    AGTNotebook *exs = [AGTNotebook notebookWithName:@"Ex-novias para el recuerdo" context:self.stack.context];
    
    
    [AGTNote noteWithName:@"Mariana Dávalos"
                 notebook:exs
                  context:self.stack.context];
    
    [AGTNote noteWithName:@"Camila Dávalos"
                 notebook:exs
                  context:self.stack.context];
    
    [AGTNote noteWithName:@"Pampita"
                 notebook:exs
                  context:self.stack.context];

    
    AGTNote *vega = [AGTNote noteWithName:@"María Teresa de la Vega"
                                 notebook:exs
                                  context:self.stack.context];

    
    NSLog(@"Una nota: %@", vega);
    
    
    // Buscar
    NSFetchRequest *req = [NSFetchRequest
                           fetchRequestWithEntityName:[AGTNote entityName]];
    
    req.sortDescriptors = @[[NSSortDescriptor
                             sortDescriptorWithKey:AGTNoteAttributes.name
                             ascending:YES
                             selector:@selector(caseInsensitiveCompare:)],
                            [NSSortDescriptor
                             sortDescriptorWithKey:AGTNoteAttributes.modificationDate
                             ascending:NO]];
    req.fetchBatchSize = 20;
    req.predicate = [NSPredicate predicateWithFormat:@"notebook = %@", exs];
    
    NSArray *results = [self.stack
                        executeFetchRequest:req
                        errorBlock:^(NSError *error) {
                            NSLog(@"error al buscar! %@", error);
                        }];
    
    NSLog(@"Notas: %@", results);
    
    // Borrar
    [self.stack.context deleteObject:vega];
    
    
    // Guardar
    [self.stack saveWithErrorBlock:^(NSError *error) {
        NSLog(@"¡Error al guardar! %@", error);
    }];
    
    
}


-(void) autoSave{
    
    if (AUTO_SAVE) {
        NSLog(@"Autoguardando");
        [self.stack saveWithErrorBlock:^(NSError *error) {
            NSLog(@"Error al autoguardar!");
        }];
        
        // Pongo en mi "agenda" una nueva llamada a autoSave
        [self performSelector:@selector(autoSave)
                   withObject:nil
                   afterDelay:AUTO_SAVE_DELAY];

    }
}













@end
