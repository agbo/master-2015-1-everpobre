//
//  AGTNotesViewController.m
//  Everpobre
//
//  Created by Fernando Rodríguez Romero on 09/04/15.
//  Copyright (c) 2015 Agbo. All rights reserved.
//

#import "AGTNotesViewController.h"
#import "AGTNote.h"
#import "AGTPhoto.h"
#import "AGTNotebook.h"
#import "AGTNoteViewController.h"

@interface AGTNotesViewController ()
@property (nonatomic, strong) AGTNotebook *notebook;
@end

@implementation AGTNotesViewController

-(id) initWithFetchedResultsController:(NSFetchedResultsController *)aFetchedResultsController
                                 style:(UITableViewStyle)aStyle
                              notebook:(AGTNotebook *) notebook{
    
    if (self = [super initWithFetchedResultsController:aFetchedResultsController
                                                 style:aStyle]) {
        _notebook = notebook;
        self.title = self.notebook.name;
    }
    return self;
}

-(void) viewDidLoad{
    [super viewDidLoad];
    
    
    UIBarButtonItem *add = [[UIBarButtonItem alloc]
                            initWithBarButtonSystemItem:UIBarButtonSystemItemAdd
                            target:self action:@selector(addNewNote:)];
    
    self.navigationItem.rightBarButtonItem = add;

}

-(void) addNewNote:(id) sender{
    
    [AGTNote noteWithName:@"Nueva nota"
                 notebook:self.notebook
                  context:self.notebook.managedObjectContext];
    
}
// el método que genera la celda
-(UITableViewCell *) tableView:(UITableView *)tableView
         cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    
    // Averiguar la nota
    AGTNote *n = [self.fetchedResultsController objectAtIndexPath:indexPath];
    
    
    // Crear la celda
    static NSString *noteCellId = @"NoteCellId";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:noteCellId];
    if (cell == nil) {
        cell = [[UITableViewCell alloc]
                initWithStyle:UITableViewCellStyleDefault
                reuseIdentifier:noteCellId];
    }
    // Sincornizar nota -> celda
    cell.imageView.image = n.photo.image;
    cell.textLabel.text = n.name;
    
    // devolverla
    return cell;
    
}


-(void) tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath{
    
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        AGTNote *n = [self.fetchedResultsController objectAtIndexPath:indexPath];
        
        [self.fetchedResultsController.managedObjectContext deleteObject:n];
    }
}



-(void) tableView:(UITableView *)tableView
didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    
    // Averiguar la nota
    AGTNote *note = [self.fetchedResultsController objectAtIndexPath:indexPath];
    
    // Crear el controlador
    AGTNoteViewController *nVC = [[AGTNoteViewController alloc] initWithModel:note];
    
    // Hacer el push
    [self.navigationController pushViewController:nVC
                                         animated:YES];
    
    
}





@end
