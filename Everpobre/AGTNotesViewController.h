//
//  AGTNotesViewController.h
//  Everpobre
//
//  Created by Fernando Rodríguez Romero on 09/04/15.
//  Copyright (c) 2015 Agbo. All rights reserved.
//

@class AGTNotebook;

#import "AGTCoreDataTableViewController.h"

@interface AGTNotesViewController : AGTCoreDataTableViewController


-(id) initWithFetchedResultsController:(NSFetchedResultsController *)aFetchedResultsController
                                 style:(UITableViewStyle)aStyle
                              notebook:(AGTNotebook *) notebook;

@end
